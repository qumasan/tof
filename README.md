# ChronoTracks : Measuring Cosmic Muons

![](./mystmd/_static/chronotracks.webp)

探Qプロジェクト用のリポジトリです。
（チーム名が決まったらリポジトリ名を変更します）

Choose a self-explaining name for your project.

## Description

[加速キッチン](https://accel-kitchen.com/)のプロジェクトのひとつとして、高校生と一緒に宇宙線測定をしています。

2台のCosmic Watchと1台のRedPitayaを使って宇宙線を検出しています。
検出器間の距離を変えて、それぞれの検出器の到来時間差を計算して、宇宙線の速度を速度を測定します。

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Getting started

```console
$ brew install git
$ brew install rye
$ brew install --cask visual-studio-code
```

このリポジトリを使うのに必要な外部パッケージです。
Gitはリポジトリにあるファイルをバージョン管理するために利用します。
RyeはPythonを使った開発環境を構築するために利用します。
Visual Studio Codeエディターを使って、解析スクリプトなどを作成します。

## Installation

```console
$ git clone https://gitlab.com/qumasan/tof.git
$ cd tof
$ rye sync
```

このリポジトリをクローンします。
``rye``を使って必要なPythonパッケージを追加して、
開発環境を準備します。

## Sync data

```console
$ cd tof
$ bash sync.sh
// or
$ rsync -auvz --delete --exclude-from="ignore_sync.txt" rp:/home/jupyter/RedPitaya/nagoya .
```

RedPitayaにある測定データをローカルPCにコピーしてください。
``-auvz``は基本的につけておくとよいオプションです。
``--delete``で、ローカルのデータをRedPitaya内のデータと完全に同期できます。
``--exclude-from="ignore_sync.txt"``で除外するファイルを指定しています。

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

```console
$ cd mystmd/daq/
$ jupyter --output single_daq.md --to myst DAQのファイル.ipynb
[jupytext] Reading DAQファイル.ipynb in format ipynb
[jupytext] Writing single_daq.md in format md:myst (destination file replaced)
```

RedPitayaの中で更新しているDAQ用ノートブックは、
jupytextでmdファイルに変換して差分管理しています。


State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
