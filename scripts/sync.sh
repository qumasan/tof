#! /usr/bin/env bash

# RedPitayaと同期するスクリプト

echo ""
echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
echo ""

# 1.
# RedPitayaにあるデータを本物とする
# ローカル（nagoya）に保存するデータは、その完全コピーにする
echo "Sync from RedPitaya..."
echo ""
rsync -auvz --delete --exclude-from="ignore_sync.txt" rp:/home/jupyter/RedPitaya/nagoya/ nagoya/
echo ""
echo "... sync finished"

echo ""
echo "--------------------------------------------------"
echo ""

# 2.
# フルバックアップを作成する
# ローカル（nagoya/）のデータをバックアップ（nagoya-backups/）に同期する
echo "Take full backups..."
echo ""
rsync -auvz --exclude-from="ignore_sync.txt" nagoya/ nagoya-backups/
echo ""
echo "... backup finished"

# 3.
# NextCloudにアップする
echo "Upload to NextCloud..."
echo ""
rsync -auvz --exclude-from="ignore_sync.txt" nagoya/data_v12 nextcloud/
echo ""
echo "... upload finished"

echo ""
echo "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
echo ""
