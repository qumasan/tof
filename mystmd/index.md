---
title: ToF Measurement of Cosmic Muon
date: 2024-01-01
---

探Qプロジェクト用のリポジトリです。
（チーム名が決まったらリポジトリ名を変更します）

[加速キッチン](https://accel-kitchen.com/)のプロジェクトのひとつとして、高校生と一緒に宇宙線測定をしています。

Cosmic Watchを2台利用し、検出時間差を使って宇宙線の速度を測定します。

## Google Colaboratory

測定データの解析はブラウザ上でPython環境を利用できる[Google Colaboratory](https://colab.research.google.com/)（通称：Colab）を使います。

Colabのファイルは**ノートブック**と呼ばれ、
Pythonコードを入力できる**コードセル**と、
Markdown記法を使って文章を入力できる**Markdownセル**で構成されています。
これらのセルを上手に組み合わせると、測定や解析のログブックとして利用できます。

## このリポジトリについて

このリポジトリは、ローカルでJupyter Notebookを実行できるようにしてあります。
GitLabからリポジトリをクローンして自由に使ってください。
必要なパッケージなどのセットアップは下記の手順を参照してください。

### リポジトリをクローンする

```console
$ git clone https://gitlab.com/qumasan/tof/
$ cd tof
```

### Pythonパッケージをインストールする

```console
$ poetry install --no-root
```

### 開発用の仮想環境を起動する

```console
$ poetry shell
(.venv) $ code .
```

